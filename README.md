![tandem-innovation](tandem-innovation.png)

# android-kotlin-mapHelper(beta)
Maphelper is designed to make it easy to implement the Google Maps Android API.
Latest version: 1.0.1 (22/10/2018) minApi: 21

Features
------------
* Markers with multiple properties (visited, activated, activation radius, etc);
* Multiples icons for each marker state;
* Polygons;
* Polylines;

Gradle setup
------------
**Project gradle:**
```
repositories {
      maven { url "https://dl.bintray.com/tandem/com.tandeminnovation.android" }
    }
```
**Module gradle:**
```
dependencies {
    implementation 'com.tandeminnovation.android:maphelper:1.0.1'
}
```
Usage
------------


License
-------
    Copyright 2018 Tandem Innovation. All rights reserved.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
