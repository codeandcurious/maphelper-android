/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.config

import android.content.Context
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.annotation.ColorInt

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.tandeminnovation.maphelper.R
import com.tandeminnovation.maphelper.helper.MapHelper
import com.tandeminnovation.maphelper.manager.MapPoiManager
import com.tandeminnovation.maphelper.manager.MapPolygonManager
import com.tandeminnovation.maphelper.manager.MapPolylineManager
import com.tandeminnovation.maphelper.Constants
import com.tandeminnovation.maphelper.behavior.MapBehavior
import com.tandeminnovation.maphelper.listener.MapListener

/**
 * Created by firetrap on 28/10/2016.
 */
class MapConfig {

    val context: Context
    //The default location is Lisbon
    var defaultLocation: Location? = null
        get() {
            if (field == null) {
                this.defaultLocation = Location("Default - Lisbon")
                field!!.latitude = DEFAULT_COORDINATES.latitude
                field!!.longitude = DEFAULT_COORDINATES.longitude
            }

            return field
        }
    var googleMap: GoogleMap? = null
    var mapListener: MapListener? = null
    var mapView: MapView? = null
        private set
    var infoWindowAdapter: GoogleMap.InfoWindowAdapter? = null
        private set
    var mapBehavior: MapBehavior? = null
    var mapHelper: MapHelper? = null
    private var savedInstanceState: Bundle? = null
    var mapPoiManager: MapPoiManager<Any>? = null
    var mapPolylineManager: MapPolylineManager<Any>? = null
    var mapPolygonManager: MapPolygonManager<Any>? = null

    var mapMaxZoom = 21.0f
    var mapMinZoom = 0.0f
    var isCenterOnUserEnabled = false
        private set
    var isCompassEnabled = true
    var isZoomOutUntilNearestPoiEnabled = true
        private set
    var defaultZoomLevel = DEFAULT_ZOOM_LEVEL
    var isPinActivationEnabled = false
    var isPinVisitEnabled = false
    val mapContainer: View
    var backgroundColorAttr: Int = 0
        private set
    var accentColorAttr: Int = 0
    var locationUpdateInterval = Constants.DEFAULT_LOCATION_UPDATE_INTERVAL.toLong()
    var fastestLocationUpdateInterval = locationUpdateInterval / 2
    var locationPriority = Constants.DEFAULT_LOCATION_PRIORITY

    val isCenteredOnUser: Boolean
        get() = mapHelper!!.isCenteredOnUser

    var isCameraTrackingEnable: Boolean
        get() = mapHelper!!.isCameraTrackingUser
        set(cameraTrackingEnable) {

            mapHelper!!.isCameraTrackingUser = cameraTrackingEnable
        }

    val mapCenter: LatLng
        get() = mapHelper!!.mapCenter

    var accuracyOffset: Float
        get() = mapHelper!!.accuracyOffset
        set(accuracyOffset) {

            mapHelper!!.accuracyOffset = accuracyOffset
        }

    val lastLocation: Location?
        get() = mapHelper!!.locationHelper.location

    constructor(mapContainer: View, mapListener: MapListener, savedInstanceState: Bundle?) {
        this.mapContainer = mapContainer
        this.mapView = mapContainer.findViewById(R.id.mapView)
        this.context = mapContainer.context
        this.mapListener = mapListener
        this.savedInstanceState = savedInstanceState
        this.setupDefaultMapBehaviour(this.savedInstanceState)
    }

    constructor(mapContainer: View, mapListener: MapListener, infoWindowAdapter: GoogleMap.InfoWindowAdapter, savedInstanceState: Bundle?) {
        this.mapContainer = mapContainer
        this.mapView = mapContainer.findViewById(R.id.mapView)
        this.context = mapView!!.context
        this.mapListener = mapListener
        this.infoWindowAdapter = infoWindowAdapter
        this.savedInstanceState = savedInstanceState
        this.setupDefaultMapBehaviour(this.savedInstanceState)
    }

    constructor(mapContainer: View, mapListener: MapListener, mapBehavior: MapBehavior, infoWindowAdapter: GoogleMap.InfoWindowAdapter) {
        this.mapContainer = mapContainer
        this.mapView = mapContainer.findViewById(R.id.mapView)
        this.context = mapView!!.context
        this.mapListener = mapListener
        this.mapBehavior = mapBehavior
        this.infoWindowAdapter = infoWindowAdapter
    }

    private fun setupDefaultMapBehaviour(savedInstanceState: Bundle?) {
        this.mapBehavior = MapBehavior(mapHelper!!, this, context, savedInstanceState)
        this.mapBehavior!!.isPinVisitEnabled = this.isPinVisitEnabled
        this.mapBehavior!!.isPinActivationEnabled = this.isPinActivationEnabled
        this.mapBehavior!!.setZoomOutUntilNearestPoi(this.isZoomOutUntilNearestPoiEnabled)
    }

    fun setCenterOnUser(centerOnUser: Boolean, @ColorInt backgroundColorAttr: Int, @ColorInt accentColorAttr: Int) {
        this.isCenterOnUserEnabled = centerOnUser
        this.backgroundColorAttr = backgroundColorAttr
        this.accentColorAttr = accentColorAttr
    }

    fun setZoomOutUntilNearestPoi(zoomOutUntilNearestPoi: Boolean) {

        this.isZoomOutUntilNearestPoiEnabled = zoomOutUntilNearestPoi

        if (this.mapBehavior != null) {

            this.mapBehavior!!.setZoomOutUntilNearestPoi(this.isZoomOutUntilNearestPoiEnabled)
        }

    }

    companion object {

        private val DEFAULT_ZOOM_LEVEL = 18.0f
        private val DEFAULT_COORDINATES = LatLng(38.749799, -9.137905) //Lisbon coordinates
    }
}
