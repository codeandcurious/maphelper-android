/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.task

import android.os.AsyncTask
import com.google.android.gms.maps.GoogleMap
import com.tandeminnovation.appbase.helper.LogHelper
import com.tandeminnovation.maphelper.helper.MapHelper
import com.tandeminnovation.maphelper.behavior.MapBehavior
import com.tandeminnovation.maphelper.model.MapPolyline

/**
 * Created by firetrap on 12/10/16.
 */
class MapPolylineAsyncTask(private val mapBehavior: MapBehavior) : AsyncTask<MapPolyline, MapPolyline, Void>() {

    private val mapHelper: MapHelper?
    var logHelper = LogHelper.instance
    private val googleMap: GoogleMap?


    init {
        this.mapHelper = mapBehavior.mapHelper
        this.googleMap = mapBehavior.googleMap
    }

    override fun doInBackground(vararg mapPolylines: MapPolyline): Void? {
        for (mapPolyline in mapPolylines) {

            publishProgress(mapPolyline)

            // Escape early if cancel() is called
            if (isCancelled) break
        }

        return null
    }

    override fun onCancelled() {
        super.onCancelled()
        logHelper.info(TAG, "Asynctask polylines canceled")
    }

    override fun onProgressUpdate(vararg values: MapPolyline) {
        val mapPolyline = values[0]
        val polyline = googleMap!!.addPolyline(mapPolyline.polylineOptions)
        mapPolyline.polyline = polyline

        mapHelper!!.getPolylineByMapPolylineId().put(mapPolyline.id, polyline)
        mapHelper.getMapPolylineByPolylineId().put(polyline.id, mapPolyline)
    }

    companion object {

        private const val TAG = "MarkersTask"
    }
}
