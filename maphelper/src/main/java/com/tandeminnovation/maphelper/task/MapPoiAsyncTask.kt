/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.task

import android.location.Location
import android.os.AsyncTask
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.tandeminnovation.appbase.helper.LogHelper
import com.tandeminnovation.maphelper.helper.LocationHelper
import com.tandeminnovation.maphelper.helper.MapHelper
import com.tandeminnovation.maphelper.behavior.MapBehavior
import com.tandeminnovation.maphelper.helper.MarkerHelper
import com.tandeminnovation.maphelper.model.MapPoi

/**
 * Created by firetrap on 14/12/2015.
 */
class MapPoiAsyncTask(mapBehavior: MapBehavior, selectedMapPoiId: Long?) : AsyncTask<MapPoi, MapPoi, MapPoi>() {

    private var selectedMapPoiId: Long? = null

    private var mapHelper: MapHelper? = null
    private var mapBehavior: MapBehavior? = null

    private val locationHelper = LocationHelper.instance
    var logHelper = LogHelper.instance
    private var googleMap: GoogleMap? = null
    private var userLocation: Location? = null

    init {
        this.mapBehavior = mapBehavior
        this.mapHelper = mapBehavior.mapHelper
        this.googleMap = mapBehavior.googleMap
        this.selectedMapPoiId = selectedMapPoiId
    }

    override fun onPreExecute() {
        super.onPreExecute()

        userLocation = locationHelper.location
    }

    override fun doInBackground(vararg mapPois: MapPoi): MapPoi? {
        var nearestPoi: MapPoi? = null
        if (mapPois.isNotEmpty()) {

            nearestPoi = mapPois[0]
            for (mapPoi in mapPois) {

                if (selectedMapPoiId != null && selectedMapPoiId!!.toLong() == mapPoi.id) {

                    mapPoi.isSelected = true
                    selectedMapPoiId = null
                }

                publishProgress(mapPoi)

                // Escape early if cancel() is called
                if (isCancelled) break
            }
        }

        return nearestPoi
    }

    override fun onCancelled() {
        super.onCancelled()
        logHelper.info(TAG, "SetMarkers Canceled")
    }

    override fun onProgressUpdate(vararg values: MapPoi) {
        addMarkers(values[0])
    }

    private fun addMarkers(mapPoi: MapPoi) {
        val markerOptions = MarkerOptions()
        markerOptions.position(LatLng(mapPoi.location!!.latitude, mapPoi.location!!.longitude))
        markerOptions.title(mapPoi.title)
        markerOptions.anchor(mapPoi.getAnchor()[0], mapPoi.getAnchor()[1])

        setMapPoiActivationState(mapPoi)

        if (mapPoi.isActive) {

            //Set unselected or selected icon
            markerOptions.icon(if (mapPoi.isSelected) mapPoi.getSelectedIcon() else mapPoi.getUnselectedIcon())
        } else {

            markerOptions.icon(mapPoi.getInactiveIcon())
        }

        val marker: Marker
        try {
            marker = googleMap!!.addMarker(markerOptions)
            mapPoi.marker = marker
        } catch (e: IllegalArgumentException) {

            throw IllegalArgumentException("MarkerOptions doesn't support vectorDrawable")
        }

        mapHelper!!.getMarkersByPoiId().put(mapPoi.id, marker)
        mapHelper!!.getMapPoiByMarkerId().put(marker.id, mapPoi)
        googleMap!!.setInfoWindowAdapter(mapPoi.infoWindowAdapter)
        if (mapPoi.isSelected) {

            mapHelper!!.selectedMarker = marker
            mapHelper!!.showInfoWindow(mapHelper!!.selectedMarker, mapPoi)
        }
    }

    private fun setMapPoiActivationState(mapPoi: MapPoi) {
        if (mapPoi.activationRadius != null && mapPoi.activationRadius != 0.0f) {

            val distance = MarkerHelper.getDistance(mapPoi.location!!, userLocation!!)
            if (distance <= mapPoi.activationRadius!!) {

                //if is inactive, active
                if (!mapPoi.isActive) {

                    mapPoi.isActive = true
                }
            } else {

                //if is active, inactive
                if (mapPoi.isActive) {

                    mapPoi.isActive = false
                }
            }
        }
    }

    override fun onPostExecute(mapPoi: MapPoi?) {
        //If savedInstanceState is null it's the first time placing markers and do the default zoomBehaviour
        if (mapPoi != null && mapBehavior!!.savedInstanceState == null) {

            mapBehavior!!.onPostExecuteBehaviour(mapPoi)
        }
    }

    companion object {

        private val TAG = "MarkersTask"
    }
}
