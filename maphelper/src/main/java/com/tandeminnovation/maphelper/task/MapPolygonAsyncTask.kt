/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.task

import android.os.AsyncTask
import com.google.android.gms.maps.GoogleMap
import com.tandeminnovation.appbase.extension.whenNotNull
import com.tandeminnovation.appbase.helper.LogHelper
import com.tandeminnovation.maphelper.helper.MapHelper
import com.tandeminnovation.maphelper.behavior.MapBehavior
import com.tandeminnovation.maphelper.model.MapPolygon

/**
 * Created by firetrap on 12/10/16.
 */
class MapPolygonAsyncTask(private val mapBehavior: MapBehavior) : AsyncTask<MapPolygon, MapPolygon, Void>() {

    private val mapHelper: MapHelper?

    var logHelper = LogHelper.instance
    private val googleMap: GoogleMap?


    init {
        this.mapHelper = mapBehavior.mapHelper
        this.googleMap = mapBehavior.googleMap
    }

    override fun doInBackground(vararg mapPolygons: MapPolygon): Void? {
        for (mapPolygon in mapPolygons) {

            publishProgress(mapPolygon)

            // Escape early if cancel() is called
            if (isCancelled) break
        }

        return null
    }

    override fun onCancelled() {
        super.onCancelled()
        logHelper.info(TAG, "Asynctask polylines canceled")
    }

    override fun onProgressUpdate(vararg values: MapPolygon) {
        val mapPolygon = values[0]
        val polygon = googleMap!!.addPolygon(mapPolygon.polygonOptions)
        mapPolygon.polygon = polygon

        whenNotNull(mapHelper) { mapHelper: MapHelper ->

            mapHelper.getPolygonByMapPolygonId()[mapPolygon.id] = polygon
            mapHelper.getMapPolygonByPolygonId()[polygon.id] = mapPolygon
        }
    }

    companion object {

        private val TAG = "MarkersTask"
    }
}
