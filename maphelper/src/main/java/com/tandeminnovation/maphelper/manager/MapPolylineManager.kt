/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.manager

import android.content.Context

import com.tandeminnovation.maphelper.adapter.MapPolylineAdapter
import com.tandeminnovation.maphelper.model.MapPolyline

/**
 * Created by firetrap on 24/10/2016.
 * Thanks Rui Milagaia to clarify me how to do it! :D
 */
class MapPolylineManager<T>(protected val context: Context, private val mapPolylineAdapter: MapPolylineAdapter<T>) {

    private val mLock = Any()

    var `object`: T? = null
        private set
    private var dataObserver: MapPolylineDataObserver? = null

    fun setDataObserver(dataObserver: MapPolylineDataObserver) {
        this.dataObserver = dataObserver
    }

    fun transform(`object`: T): MapPolyline {

        return mapPolylineAdapter.transform(`object`)
    }

    fun add(`object`: T?) {
        synchronized(mLock) {

            this.`object` = `object`
        }
    }

    fun notifyDataSetChanged() {

        dataObserver!!.dataSetChanged()
    }

    interface MapPolylineDataObserver {

        fun dataSetChanged()
    }

    companion object {

        private const val TAG = "MapPolylineManager"
    }
}