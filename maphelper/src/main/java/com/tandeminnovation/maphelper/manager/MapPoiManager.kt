/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.manager

import android.content.Context
import com.tandeminnovation.appbase.helper.LogHelper
import com.tandeminnovation.maphelper.adapter.MapPoiAdapter
import com.tandeminnovation.maphelper.model.MapPoi
import java.util.*

/**
 * Created by firetrap on 28/10/2016.
 * Thanks Rui Milagaia to clarify me how to do it! :D
 */
class MapPoiManager<T>(protected val context: Context, private val mapPoiAdapter: MapPoiAdapter<T>) {

    private val mLock = Any()

    private val mutableListOfObjects = mutableListOf<T>()
    private var dataObserver: MapPoiDataObserver? = null
    var logHelper = LogHelper.instance

    val isEmpty: Boolean
        get() = mutableListOfObjects.isEmpty()

    val objects: List<T>
        get() = mutableListOfObjects

    fun setDataObserver(dataObserver: MapPoiDataObserver) {
        this.dataObserver = dataObserver
    }

    fun transform(objectsToTransform: List<T>): List<MapPoi> {
        val result = ArrayList<MapPoi>()
        for (transformer in objectsToTransform) {

            val mapPoi = mapPoiAdapter.transform(transformer)
            result.add(mapPoi)
        }

        return result
    }

    fun set(`object`: T?) {
        synchronized(mLock) {

            mutableListOfObjects.clear()
            mutableListOfObjects.add(`object`!!)
        }
    }

    fun setAll(collection: Collection<T>) {
        synchronized(mLock) {

            mutableListOfObjects.clear()
            mutableListOfObjects.addAll(collection)
        }
    }

    fun setAll(vararg items: T) {
        synchronized(mLock) {

            mutableListOfObjects.clear()
            Collections.addAll(mutableListOfObjects, *items)
        }
    }

    fun add(`object`: T) {
        synchronized(mLock) {

            mutableListOfObjects.add(`object`)
        }
    }

    fun addAll(collection: Collection<T>) {
        synchronized(mLock) {

            mutableListOfObjects.addAll(collection)
        }
    }

    fun addAll(vararg objects: T) {
        synchronized(mLock) {

            Collections.addAll(mutableListOfObjects, *objects)
        }
    }

    fun insert(`object`: T, index: Int) {
        synchronized(mLock) {

            mutableListOfObjects.add(index, `object`)
        }
    }

    fun remove(`object`: T?) {
        synchronized(mLock) {

            mutableListOfObjects.remove(`object`)
        }
    }

    fun clear() {
        synchronized(mLock) {

            mutableListOfObjects.clear()
        }
    }

    fun notifyDataSetChanged() {

        dataObserver!!.dataSetChanged()
    }

    interface MapPoiDataObserver {

        fun dataSetChanged()
    }

    companion object {

        private const val TAG = "MapPoiManager"
    }
}