/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.manager

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.tandeminnovation.appbase.helper.LogHelper
import com.tandeminnovation.maphelper.helper.LocationHelper
import com.tandeminnovation.maphelper.helper.MapHelper
import com.tandeminnovation.maphelper.Constants
import com.tandeminnovation.maphelper.behavior.MapBehavior
import com.tandeminnovation.maphelper.config.MapConfig

/**
 * Created by firetrap on 14/12/2015.
 */
class MapManager(private val appCompatActivity: AppCompatActivity, mapConfig: MapConfig, private val savedInstanceState: Bundle) {

    private var mGoogleMap: GoogleMap? = null
    private var mapView: MapView? = null
    private var mapConfig: MapConfig? = null
    private var mapHelper: MapHelper
    var logHelper = LogHelper.instance
    private val locationHelper = LocationHelper.instance
    private var mapBehavior: MapBehavior? = null
    private var isStarted = false
    private var isResumed = false

    init {
        this.mapConfig = mapConfig
        this.mapView = mapConfig.mapView
        this.mapHelper = MapHelper()
        this.mapConfig!!.mapHelper = mapHelper
        this.setupLocationHelper(appCompatActivity, savedInstanceState)
    }

    private fun setupLocationHelper(appCompatActivity: AppCompatActivity, savedInstanceState: Bundle) {
        this.locationHelper.locationUpdateInterval = this.mapConfig!!.locationUpdateInterval
        this.locationHelper.fastestLocationUpdateInterval = this.mapConfig!!.fastestLocationUpdateInterval
        this.locationHelper.locationPriority = this.mapConfig!!.locationPriority
        this.locationHelper.defaultLocation = mapConfig!!.defaultLocation
        this.locationHelper.addOnLocationHelperListener(mapHelper.locationHelperListener)
        this.locationHelper.setup(appCompatActivity, savedInstanceState)
    }

    fun onStart() {

        locationHelper.onStart()
        isStarted = true
    }

    fun onStop() {

        locationHelper.onStop()
    }

    fun onResume() {

        mapView!!.onResume()
        isResumed = true
    }

    fun onPause() {

        mapView!!.onPause()
    }

    fun onLowMemory() {

        mapView!!.onLowMemory()
    }

    fun onDestroy() {

        mapView!!.onDestroy()
    }

    fun onSaveInstanceState(outState: Bundle) {
        if (mGoogleMap != null) {

            saveCameraPositionState(outState)
            saveSelectedMarkerIdState(outState)
        }
    }

    private fun saveCameraPositionState(outState: Bundle) {
        outState.putParcelable(Constants.CAMERA_POSITION_STATE_KEY, mGoogleMap!!.cameraPosition)
    }

    private fun saveSelectedMarkerIdState(outState: Bundle) {
        if (mapHelper.selectedMarker != null) {

            val mapPoi = mapHelper.getMapPoiByMarkerId()[mapHelper.selectedMarker!!.id]
            if (mapPoi != null) {

                outState.putLong(Constants.SELECTED_MAPPOI_ID_STATE_KEY, mapPoi.id)
            }
        }
    }

    fun setupMap() {
        MapsInitializer.initialize(mapView!!.context)
        mapView!!.onCreate(savedInstanceState)
        mapView!!.getMapAsync { googleMap ->
            checkState(isStarted, "Had you done 'mapManager.onStart()' on your fragment/appCompatActivity onStart() ?")
            checkState(isResumed, "Had you done 'mapManager.onResume()' on your fragment/appCompatActivity onResume() ?")

            mGoogleMap = googleMap

            //MapConfig setup
            setupMapConfig()

            //MapHelper setup
            setupMapHelper()

            //MapBehavior setup
            setupMapBehavior()
        }
    }

    private fun setupMapConfig() {
        mapConfig!!.googleMap = mGoogleMap
    }

    private fun setupMapHelper() {
        mapHelper.setup(mapConfig!!)
    }

    private fun setupMapBehavior() {
        mapBehavior = mapConfig!!.mapBehavior
        mapBehavior!!.mapHelper = mapHelper
        mapBehavior!!.googleMap = mGoogleMap

        if (mapConfig!!.mapPoiManager != null) {

            mapBehavior!!.mapPoiManager = mapConfig!!.mapPoiManager
        }

        if (mapConfig!!.mapPolylineManager != null) {

            mapBehavior!!.mapPolylineManager = mapConfig!!.mapPolylineManager
        }

        if (mapConfig!!.mapPolygonManager != null) {

            mapBehavior!!.mapPolygonManager = mapConfig!!.mapPolygonManager
        }
    }

    fun clearMapMarkers() {

        mapHelper.clearMapMarkers()
    }

    fun clearMapPolygons() {

        mapHelper.clearMapPolygons()
    }

    fun clearMapPolylines() {

        mapHelper.clearMapPolylines()
    }

    fun clearMap() {

        mapHelper.clearMap()
    }


    private fun checkState(expression: Boolean, errorMessage: Any?) {
        if (!expression) throw IllegalStateException(errorMessage.toString())
    }

    companion object {

        private const val TAG = "MapManager"
    }

    //    public void applyBehavior() {
    //
    //        mapBehavior.onPreExecuteMapPoiBehavior();
    //    }
}