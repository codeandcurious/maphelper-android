/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.helper

import android.graphics.Point
import android.location.Location

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker

/**
 * Created by firetrap on 12/10/16.
 */
object MarkerHelper {

    /**
     * Gets visible bounds.
     *
     * @param googleMap the google map
     * @return the visible bounds
     */
    fun getVisibleBounds(googleMap: GoogleMap): LatLngBounds {

        return googleMap.projection.visibleRegion.latLngBounds
    }

    /**
     * Gets distance between locations.
     *
     * @param from the from
     * @param to   the to
     * @return the distance between locations
     */
    fun getDistance(from: Location, to: Location): Float {

        return from.distanceTo(to)
    }

    /**
     * Get distance between 2 Location in meters
     *
     * @param from the from position
     * @param to   the to position
     * @return distance(meters) distance
     */
    fun getDistance(from: LatLng, to: LatLng): Float {
        val fromLocation = Location("")
        fromLocation.latitude = from.latitude
        fromLocation.longitude = from.longitude

        val toLocation = Location("")
        toLocation.latitude = to.latitude
        toLocation.longitude = to.longitude

        return fromLocation.distanceTo(toLocation)
    }

    /**
     * Deg 2 rad double.
     *
     * @param deg the deg
     * @return the double
     */
    fun deg2rad(deg: Double): Double {
        return deg * Math.PI / 180.0
    }

    /**
     * Rad 2 deg double.
     *
     * @param rad the rad
     * @return the double
     */
    fun rad2deg(rad: Double): Double {
        return rad * 180.0 / Math.PI
    }

    /**
     * Returns the screen location based on the anchor point (center of the
     * marker) of given marker
     *
     * @param mMap   the m map
     * @param marker the marker
     * @return marker screen location
     */
    fun getMarkerScreenLocation(mMap: GoogleMap, marker: Marker): Point {
        val projection = mMap.projection
        val markerLocation = marker.position

        return projection.toScreenLocation(markerLocation)
    }

    /**
     * Returns latitude string formatted with cardinal points
     *
     * @param latitude the latitude
     * @return latitude cardinal
     */
    fun getLatitudeCardinal(latitude: Double): String {
        val cardinal = java.lang.Double.toString(latitude).replace(".", "º")
        return if (latitude >= 0.0) "$cardinal N" else "$cardinal S"
    }

    /**
     * Returns latitude string formatted with cardinal points
     *
     * @param latitudeText the latitude text
     * @return latitude cardinal
     * @throws Exception the exception
     */
    @Throws(Exception::class)
    fun getLatitudeCardinal(latitudeText: String): String {
        val latitude = java.lang.Double.parseDouble(latitudeText)
        val cardinal = java.lang.Double.toString(latitude).replace(".", "º")
        return if (latitude >= 0.0) "$cardinal N" else "$cardinal S"
    }

    /**
     * Returns longitude string formatted with cardinal points
     *
     * @param longitude the longitude
     * @return longitude cardinal
     */
    fun getLongitudeCardinal(longitude: Double): String {
        val cardinal = java.lang.Double.toString(longitude).replace(".", "º")
        return if (longitude >= 0.0) "$cardinal E" else "$cardinal W"
    }

    /**
     * Returns longitude string formatted with cardinal points
     *
     * @param longitudeText the longitude text
     * @return longitude cardinal
     * @throws Exception the exception
     */
    @Throws(Exception::class)
    fun getLongitudeCardinal(longitudeText: String): String {
        val longitude = java.lang.Double.parseDouble(longitudeText)
        val cardinal = java.lang.Double.toString(longitude).replace(".", "º")
        return if (longitude >= 0.0) "$cardinal E" else "$cardinal W"
    }

    /**
     * Are coordinates within limits boolean.
     *
     * @param northLimit the north limit
     * @param southLimit the south limit
     * @param eastLimit  the east limit
     * @param westLimit  the west limit
     * @param latitude   the latitude
     * @param longitude  the longitude
     * @return the boolean
     */
    fun areCoordinatesWithinLimits(northLimit: Double, southLimit: Double, eastLimit: Double, westLimit: Double, latitude: Double, longitude: Double): Boolean {

        return southLimit < latitude && northLimit > latitude && westLimit < longitude && eastLimit > longitude
    }

    /**
     * Gets distance.
     *
     * @param startLat  the start lat
     * @param startLong the start long
     * @param endLat    the end lat
     * @param endLong   the end long
     * @return the distance
     */
    fun getDistance(startLat: Double, startLong: Double, endLat: Double, endLong: Double): Float {
        val results = FloatArray(3)
        Location.distanceBetween(startLat, startLong, endLat, endLong, results)

        return results[0]
    }

    /**
     * Gets symetric coordinates.
     *
     * @param pointA the my coordinates
     * @param pointB the point a
     * @return the symetric coordinates
     */
    fun getSymmetricCoordinates(pointA: LatLng, pointB: LatLng): LatLng {

        return LatLng(pointA.latitude + (pointA.latitude - pointB.latitude), pointA.longitude + (pointA.longitude - pointB.longitude))
    }
}
