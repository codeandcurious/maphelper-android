/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.helper

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.location.Location
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import android.view.View
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.tandeminnovation.appbase.extension.whenNotNull
import com.tandeminnovation.appbase.helper.DebugHelper
import com.tandeminnovation.appbase.helper.LogHelper
import com.tandeminnovation.appbase.helper.ResourceHelper
import com.tandeminnovation.maphelper.R
import com.tandeminnovation.maphelper.config.MapConfig
import com.tandeminnovation.maphelper.listener.LocationHelperListener
import com.tandeminnovation.maphelper.listener.MapListener
import com.tandeminnovation.maphelper.model.MapPoi
import com.tandeminnovation.maphelper.model.MapPolygon
import com.tandeminnovation.maphelper.model.MapPolyline
import com.tandeminnovation.maphelper.task.MapPoiAsyncTask
import com.tandeminnovation.maphelper.task.MapPolygonAsyncTask
import com.tandeminnovation.maphelper.task.MapPolylineAsyncTask
import java.util.*

/**
 * Created by firetrap on 28/10/2016.
 */
@SuppressLint("UseSparseArrays")
class MapHelper {

    var isFirstLocationUpdate = true
    private var centerOnUserStateColor: Int = 0
    var isCameraTrackingUser = false

    private var context: Context? = null
    var selectedMarker: Marker? = null
    var googleMap: GoogleMap? = null
    var mapConfig: MapConfig? = null
        private set
    private var mapListener: MapListener? = null
    private var fabCenterOnUser: FloatingActionButton? = null

    private val mapPoiByMarkerId: MutableMap<String, MapPoi> = mutableMapOf()
    private val markersByPoiId: MutableMap<Long, Marker> = mutableMapOf()

    private val mapPolylineByPolylineId: MutableMap<String, MapPolyline> = mutableMapOf()
    private val polylineByMapPolylineId: MutableMap<Long, Polyline> = mutableMapOf()

    private val mapPolygonByPolygonId: MutableMap<String, MapPolygon> = mutableMapOf()
    private val polygonByMapPolygonId: MutableMap<Long, Polygon> = mutableMapOf()


    val markerList: List<Marker>? = null

    private val logHelper = LogHelper.instance
    var locationHelper = LocationHelper.instance
    private var mapContainer: View? = null

    var accuracyOffset = 1.0f

    val onInfoWindowClickListener: GoogleMap.OnInfoWindowClickListener = GoogleMap.OnInfoWindowClickListener { marker ->
        val mapPoi = mapPoiByMarkerId[marker.id]
        if (mapListener != null && mapPoi != null) {

            mapListener!!.onMapPoiClick(mapPoi)
        }
    }

    //Deselect other markers
    //Select current marker
    val onMarkerClickListener: GoogleMap.OnMarkerClickListener = GoogleMap.OnMarkerClickListener { marker ->
        if (marker != null) {

            val mapPoi = mapPoiByMarkerId[marker.id]
            if (mapPoi != null && mapPoi.isActive && mapListener != null) {
                setMarkerSelectionState(selectedMarker, false)
                selectedMarker = marker
                setMarkerSelectionState(marker, true)

                if (mapPoi.containsInfoWindow()) {

                    showInfoWindow(selectedMarker, mapPoi)
                } else {

                    mapListener!!.onMapPoiClick(mapPoi)
                }
            }
        }

        marker?.title == null
    }
    val onMapClickListener: GoogleMap.OnMapClickListener = GoogleMap.OnMapClickListener {
        setMarkerSelectionState(selectedMarker, false)
        selectedMarker = null
        mapListener!!.onMapClick()
    }

    //if is inactive, active
    //if is active, inactive
    //  mapConfig.getDefaultZoomLevel(), false);
    var locationHelperListener: LocationHelperListener = object : LocationHelperListener {

        override fun onLocationChanged(location: Location) {
            if (DebugHelper.isDebuggable(context)) {

                logHelper.info(TAG, "Location updated to: LAT: "
                        + location.latitude + "  LNG: " + location.longitude)
            }
            cameraTrack()
            for ((_, mapPoi) in getMapPoiByMarkerId()) {

                if (mapPoi.activationRadius != null) {

                    val distance = MarkerHelper.getDistance(mapPoi.location!!, location)
                    if (distance <= mapPoi.activationRadius!!) {
                        if (!mapPoi.isActive) {

                            mapPoi.isActive = true
                            mapPoi.marker!!.setIcon(mapPoi.getUnselectedIcon())
                        }
                    } else {
                        if (mapPoi.isActive) {

                            mapPoi.isActive = false
                            mapPoi.marker!!.setIcon(mapPoi.getInactiveIcon())
                        }
                    }
                }
            }
        }

        override fun onInitialized(bundle: Bundle) {

        }

        override fun onDisconnected() {

        }

        override fun onConnectionFailed() {

        }

        override fun onLocationSuspended(cause: Int) {

        }

        override fun onConnected() {
            whenNotNull(locationHelper.location) { location: Location ->

                if (locationHelper.location != null && isFirstLocationUpdate) {

                    isFirstLocationUpdate = false
                }

                centerCameraOnLocation(location, mapConfig!!.defaultZoomLevel, false)
            }

            mapListener!!.onMapReady()
        }
    }

    private val onCenterOnUserClick = View.OnClickListener {
        if (isCameraTrackingUser) {

            tintCenterOnUserFab(ResourceHelper.getColor(context!!, R.color.default_fab_color, null))
            isCameraTrackingUser = false
        } else {

            whenNotNull(locationHelper.location) { location: Location ->

                centerCameraOnLocation(location, mapConfig!!.defaultZoomLevel, true)
            }
            tintCenterOnUserFab(mapConfig!!.accentColorAttr)
            isCameraTrackingUser = true
        }
    }

    private val onCameraMoveListener = GoogleMap.OnCameraMoveListener { }

    private val onCameraMoveStartedListener = GoogleMap.OnCameraMoveStartedListener { reason ->
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            restoreCenterOnUserState()
            if (DebugHelper.isDebuggable(context)) {

                Toast.makeText(context, "The user gestured on the map.", Toast.LENGTH_SHORT).show()
            }
        } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION) {

            if (DebugHelper.isDebuggable(context)) {

                Toast.makeText(context, "The user tapped something on the map.", Toast.LENGTH_SHORT).show()
            }
        } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_DEVELOPER_ANIMATION) {

            if (DebugHelper.isDebuggable(context)) {

                Toast.makeText(context, "The app moved the camera.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val onCameraIdle = GoogleMap.OnCameraIdleListener { }

    val currentZoomLevel: Float
        get() = mapConfig!!.googleMap!!.cameraPosition.zoom

    val isCenteredOnUser: Boolean
        get() {
            val accurateUserLocation = locationHelper.location
            if (googleMap == null) {

                return false
            }
            if (googleMap!!.cameraPosition == null) {

                return false
            }
            val inaccurateUserLocation = Location("inaccurateUserLocation")
            whenNotNull(accurateUserLocation) { location: Location ->
                inaccurateUserLocation.latitude = location.latitude
                inaccurateUserLocation.longitude = location.longitude
            }

            val cameraLocation = Location("CameraLocation")
            cameraLocation.latitude = googleMap!!.cameraPosition.target.latitude
            cameraLocation.longitude = googleMap!!.cameraPosition.target.longitude

            return inaccurateUserLocation.distanceTo(cameraLocation) < accuracyOffset
        }

    val mapCenter: LatLng
        get() = googleMap!!.cameraPosition.target

    val locationUpdateInterval: Long
        get() = this.locationHelper.locationUpdateInterval

    var fastestLocationUpdateInterval: Long
        get() = this.locationHelper.fastestLocationUpdateInterval
        set(fastestLocationUpdateInterval) {

            this.locationHelper.fastestLocationUpdateInterval = fastestLocationUpdateInterval
        }

    var locationPriority: Int
        get() = this.locationHelper.locationPriority
        set(locationPriority) {

            this.locationHelper.locationPriority = locationPriority
        }

    internal enum class MovementType {

        CAMERA_MOVEMENT,
        LOCATION_MOVEMENT
    }

    fun setup(mapConfig: MapConfig) {
        this.mapConfig = mapConfig
        this.mapListener = mapConfig.mapListener
        this.context = mapConfig.context
        this.googleMap = mapConfig.googleMap
        this.mapContainer = mapConfig.mapContainer
        this.fabCenterOnUser = mapContainer!!.findViewById(R.id.floatingActionButtonCenterOnUser)

        setupListeners()
        setupUiSettings()
        setupCenterOnUser()
    }

    private fun setupListeners() {
        this.googleMap!!.setOnMarkerClickListener(onMarkerClickListener)
        this.googleMap!!.setOnInfoWindowClickListener(onInfoWindowClickListener)
        this.googleMap!!.setOnMapClickListener(onMapClickListener)
        this.googleMap!!.setOnCameraMoveListener(onCameraMoveListener)
        this.googleMap!!.setOnCameraMoveStartedListener(onCameraMoveStartedListener)
        this.googleMap!!.setOnCameraIdleListener(onCameraIdle)
    }

    private fun setupUiSettings() {
        this.googleMap!!.uiSettings.isMyLocationButtonEnabled = false
        this.googleMap!!.uiSettings.isZoomControlsEnabled = false
        this.googleMap!!.uiSettings.isCompassEnabled = mapConfig!!.isCompassEnabled
        this.googleMap!!.uiSettings.isMapToolbarEnabled = false
    }

    @SuppressLint("MissingPermission")
    private fun setupCenterOnUser() {
        if (mapConfig!!.isCenterOnUserEnabled) {

            if (MapHelper.checkPermissions(context!!, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)) {

                googleMap!!.isMyLocationEnabled = true
                fabCenterOnUser!!.visibility = View.VISIBLE
                fabCenterOnUser!!.setOnClickListener(onCenterOnUserClick)
                fabCenterOnUser!!.backgroundTintList = ColorStateList.valueOf(mapConfig!!.backgroundColorAttr)
            }
        }
    }

    fun getMarkersByPoiId(): MutableMap<Long, Marker> {

        return markersByPoiId
    }

    fun getMapPoiByMarkerId(): MutableMap<String, MapPoi> {

        return mapPoiByMarkerId
    }

    fun getMapPolylineByPolylineId(): MutableMap<String, MapPolyline> {

        return mapPolylineByPolylineId
    }

    fun getPolylineByMapPolylineId(): MutableMap<Long, Polyline> {

        return polylineByMapPolylineId
    }

    fun getMapPolygonByPolygonId(): MutableMap<String, MapPolygon> {

        return mapPolygonByPolygonId
    }

    fun getPolygonByMapPolygonId(): MutableMap<Long, Polygon> {

        return polygonByMapPolygonId
    }

    private fun tintCenterOnUserFab(color: Int) {
        if (centerOnUserStateColor != color) {

            centerOnUserStateColor = color
            DrawableCompat.setTint(fabCenterOnUser!!.drawable, color)
        }
    }

    private fun restoreCenterOnUserState() {
        if (mapConfig!!.isCenterOnUserEnabled) {

            val userLocation = locationHelper.location
            val cameraPosition = googleMap!!.cameraPosition.target
            whenNotNull(userLocation) { location: Location ->
                val userPosition = LatLng(location.latitude, location.longitude)

                //distance between camera and user in meters
                val distance = MarkerHelper.getDistance(cameraPosition, userPosition)

                if (distance >= 0) {

                    tintCenterOnUserFab(ResourceHelper.getColor(context!!, R.color.default_fab_color, null))
                    isCameraTrackingUser = false
                }
            }
        }
    }

    private fun cameraTrack() {
        if (isCameraTrackingUser) {

            whenNotNull(locationHelper.location) { location: Location ->
                centerCameraOnLocation(location, currentZoomLevel, true)
            }
        }
    }

    private fun setMarkerSelectionState(marker: Marker?, selected: Boolean) {
        if (marker != null) {

            val mapPoi = mapPoiByMarkerId[marker.id]
            if (mapPoi != null) {

                mapPoi.isSelected = selected
                marker.setIcon(if (selected) mapPoi.getSelectedIcon() else mapPoi.getUnselectedIcon())
            }
        }
    }

    private fun getMapPoisWithChangedActiveState(mapPoiCollection: Collection<MapPoi>, currentLocation: Location): List<MapPoi> {

        val result = ArrayList<MapPoi>()
        var distance: Float
        var poiActivationDistance: Float?

        for (mapPoi in mapPoiCollection) {

            distance = currentLocation.distanceTo(mapPoi.location)
            if (mapPoi.activationRadius == null) {

                throw NullPointerException("Poi activation radius cannot be null")
            }
            poiActivationDistance = mapPoi.activationRadius
            if (distance > poiActivationDistance!!) {
                // if not active
                if (mapPoi.isActive) {

                    mapPoi.isActive = false
                    result.add(mapPoi)
                }
            } else {

                // if active
                if (!mapPoi.isActive) {

                    mapPoi.isActive = true
                    result.add(mapPoi)
                }
            }
        }

        return result
    }

    fun addMarkers(mapPoiList: List<MapPoi>?, selectedMapPoiId: Long?) {
        if (mapPoiList != null) {

            clearMapMarkers()
            whenNotNull(locationHelper.location) { location: Location ->

                applySort(mapPoiList, location, SORT_BY_NEAREST_LOCATION)
            }
            MapPoiAsyncTask(mapConfig!!.mapBehavior!!, selectedMapPoiId).execute(*mapPoiList.toTypedArray())
        }
    }

    fun updateMarkers(mapPoiList: List<MapPoi>?, selectedMapPoiId: Long?) {
        if (mapPoiList != null) {

            whenNotNull(locationHelper.location) { location: Location ->

                applySort(mapPoiList, location, SORT_BY_NEAREST_LOCATION)
            }
            MapPoiAsyncTask(mapConfig!!.mapBehavior!!, selectedMapPoiId).execute(*mapPoiList.toTypedArray())
        }
    }

    fun addPolylinesToMap(mapPolyline: MapPolyline?) {
        if (mapPolyline != null) {

            clearMapPolylines()
            MapPolylineAsyncTask(mapConfig!!.mapBehavior!!).execute(mapPolyline)
        }
    }

    fun addPolygonToMap(mapPolygon: MapPolygon?) {
        if (mapPolygon != null) {

            //            clearMapPolygons();
            MapPolygonAsyncTask(mapConfig!!.mapBehavior!!).execute(mapPolygon)
        }
    }

    fun clearMap() {
        googleMap!!.clear()

        markersByPoiId.clear()
        mapPoiByMarkerId.clear()

        mapPolylineByPolylineId.clear()
        polylineByMapPolylineId.clear()

        mapPolygonByPolygonId.clear()
        polygonByMapPolygonId.clear()
    }

    fun clearMapMarkers() {
        for ((_, mapPoi) in mapPoiByMarkerId) {

            mapPoi.marker!!.remove()
        }

        markersByPoiId.clear()
        mapPoiByMarkerId.clear()
    }

    fun clearMapPolylines() {
        for ((_, mapPolyline) in mapPolylineByPolylineId) {

            mapPolyline.polyline!!.remove()
        }

        polylineByMapPolylineId.clear()
        mapPolylineByPolylineId.clear()
    }

    fun clearMapPolygons() {
        for ((_, mapPolygon) in mapPolygonByPolygonId) {

            mapPolygon.polygon!!.remove()
        }

        polygonByMapPolygonId.clear()
        mapPolygonByPolygonId.clear()
    }

    fun applySort(mapPois: List<MapPoi>, location: Location, sortType: Int) {
        when (sortType) {

            MapHelper.SORT_BY_NEAREST_LOCATION -> sortPoiByNearest(mapPois, location)

            MapHelper.SORT_BY_FARAWAY_LOCATION -> sortPoiByFarthest(mapPois, location)

            else -> sortPoiByNearest(mapPois, location)
        }
    }

    fun sortPoiByNearest(mapPois: List<MapPoi>, location: Location) {
        Collections.sort(mapPois, Comparator { lhs, rhs ->
            if (lhs != null && rhs == null) {

                return@Comparator -1
            } else if (lhs == null && rhs == null) {

                return@Comparator 0
            } else if (lhs == null) {

                return@Comparator 1
            }

            val distToUserLhs = MarkerHelper.getDistance(lhs.location!!, location)
            val distToUserRhs = MarkerHelper.getDistance(rhs.location!!, location)

            distToUserLhs.compareTo(distToUserRhs)
        })
    }

    fun sortPoiByFarthest(mapPois: List<MapPoi>, location: Location) {
        Collections.sort(mapPois, Comparator { lhs, rhs ->
            if (lhs != null && rhs == null) {

                return@Comparator -1
            } else if (lhs == null && rhs == null) {

                return@Comparator 0
            } else if (lhs == null) {

                return@Comparator 1
            }

            val distToUserLhs = MarkerHelper.getDistance(lhs.location!!, location)
            val distToUserRhs = MarkerHelper.getDistance(rhs.location!!, location)

            distToUserRhs.compareTo(distToUserLhs)
        })
    }

    fun centerCameraOnLocation(location: Location, zoomLevel: Float, animate: Boolean) {
        val zoomToMe = CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), zoomLevel)
        if (animate) {

            this.googleMap!!.animateCamera(zoomToMe)
        } else {

            this.googleMap!!.moveCamera(zoomToMe)
        }
    }

    fun centerCameraOnPosition(cameraPosition: CameraPosition, animate: Boolean) {
        val zoomToMe = CameraUpdateFactory.newCameraPosition(cameraPosition)
        if (animate) {

            this.googleMap!!.animateCamera(zoomToMe)
        } else {

            this.googleMap!!.moveCamera(zoomToMe)
        }
    }

    fun showInfoWindow(marker: Marker?, mapPoi: MapPoi?) {
        if (mapPoi!!.containsInfoWindow()) {

            marker!!.showInfoWindow()
        }

        if (mapPoi.containsInfoWindow() && mapPoi.infoWindowAnchor != null) {

            marker!!.setAnchor(mapPoi.infoWindowAnchor!![0], mapPoi.infoWindowAnchor!![1])
        }
    }

    fun setLocationUpdateInterval(milliseconds: Int) {

        this.locationHelper.locationUpdateInterval = milliseconds.toLong()
    }

    companion object {

        private const val TAG = "MapHelper"

        const val SORT_BY_NEAREST_LOCATION = 0
        const val SORT_BY_FARAWAY_LOCATION = 1

        public fun checkPermissions(context: Context, vararg permissions: String): Boolean {
            var result = true
            for (permission in permissions) {

                if (ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_DENIED) {

                    result = false
                }
            }

            return result
        }
    }
}
