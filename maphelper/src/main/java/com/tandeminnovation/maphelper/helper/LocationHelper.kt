/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.helper

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.IntentSender
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.tandeminnovation.appbase.helper.DebugHelper
import com.tandeminnovation.appbase.helper.LogHelper
import com.tandeminnovation.maphelper.Constants
import com.tandeminnovation.maphelper.listener.LocationHelperListener
import java.text.MessageFormat

/**
 * Created by firetrap on 28/10/2016.
 */
class LocationHelper private constructor() {

    var locationUpdateInterval = Constants.DEFAULT_LOCATION_UPDATE_INTERVAL.toLong()
    var locationPriority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
    var fastestLocationUpdateInterval = locationUpdateInterval / 2
    private var initialized = false
    private var resolvingError = false

    private var appCompatActivity: AppCompatActivity? = null
    var locationHelperListener: LocationHelperListener? = null
        private set
    private var logHelper = LogHelper.instance
    private var googleApiClient: GoogleApiClient? = null
    private var lastLocation: Location? = null
    var defaultLocation: Location? = null
    private var locationRequest: LocationRequest? = null
    private var locationManager: LocationManager? = null

    private val googlePlayServicesHelper = GooglePlayServicesHelper.instance

    private val googleApiClientConnectionCallbacks = object : GoogleApiClient.ConnectionCallbacks {

        @SuppressLint("MissingPermission")
        override fun onConnected(dataBundle: Bundle?) {
            if (MapHelper.checkPermissions(appCompatActivity!!, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)) {

                locationRequest = LocationRequest.create()
                locationRequest!!.priority = locationPriority
                locationRequest!!.interval = locationUpdateInterval
                locationRequest!!.fastestInterval = fastestLocationUpdateInterval

                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, locationListener)
            }

            this@LocationHelper.onConnected()
        }

        override fun onConnectionSuspended(cause: Int) {

            this@LocationHelper.onSuspended(cause)
        }
    }

    private val connectionFailed = GoogleApiClient.OnConnectionFailedListener { connectionResult ->
        if (DebugHelper.isDebuggable(appCompatActivity)) {

            logHelper.debug(TAG, "Google Api Client failed on connecting...")
        }

        if (resolvingError) {

            // Already attempting to resolve an error.
            return@OnConnectionFailedListener
        } else if (connectionResult.hasResolution()) {

            attemptResolveConnectionError(connectionResult)
        } else {

            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            showErrorDialog(connectionResult.errorCode)
            resolvingError = true
        }

        this@LocationHelper.onConnectionFailed()
    }

    private val locationListener = LocationListener { newLocation -> this@LocationHelper.onLocationChanged(newLocation) }

    var location: Location? = null
        @SuppressLint("MissingPermission")
        get() {

            if (checkInitialization()) {

                if (MapHelper.checkPermissions(appCompatActivity!!, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)) {

                    lastLocation = getLastLocation()

                    if (lastLocation == null) {

                        lastLocation = locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                    }

                    if (lastLocation == null) {

                        lastLocation = locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                    }
                }
            }


            if (lastLocation == null) {

                if (MapHelper.checkPermissions(appCompatActivity!!, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)) {

                    lastLocation = getLastLocation()
                }
            }

            if (lastLocation == null) {

                lastLocation = defaultLocation
            }

            return lastLocation!!
        }

    fun setup(appCompatActivity: AppCompatActivity, bundle: Bundle): Boolean {
        this.appCompatActivity = appCompatActivity

        initialized = googlePlayServicesHelper.servicesConnected(this.appCompatActivity!!)

        if (initialized) {

            setupLocationClient()

            onInitialized(bundle)

            //            setupDefaultLocation();
        }

        return initialized
    }

    //    private void setupDefaultLocation() {
    //        defaultLocation = new Location("Default - Lisbon");
    //        defaultLocation.setLatitude(38.749799f);
    //        defaultLocation.setLongitude(-9.137905f);
    //    }

    private fun setupLocationClient() {
        googleApiClient = GoogleApiClient.Builder(appCompatActivity!!)
                .addConnectionCallbacks(googleApiClientConnectionCallbacks).addApi(LocationServices.API)
                .addOnConnectionFailedListener(connectionFailed).build()
    }

    private fun setupLocationManager() {

        locationManager = appCompatActivity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    private fun attemptResolveConnectionError(connectionResult: ConnectionResult) {
        try {
            resolvingError = true
            connectionResult.startResolutionForResult(appCompatActivity, REQUEST_RESOLVE_ERROR)

        } catch (e: IntentSender.SendIntentException) {

            // There was an error with the resolution intent. Try again.
            googleApiClient!!.connect()
        }

    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation(): Location {

        return LocationServices.FusedLocationApi.getLastLocation(googleApiClient)
    }

    fun onStart(): Boolean {
        var result = false
        if (initialized) {

            if (googleApiClient == null) {

                setupLocationClient()
            }

            if (locationManager == null) {

                setupLocationManager()
            }

            if (!googleApiClient!!.isConnected && !googleApiClient!!.isConnecting) {

                googleApiClient!!.connect()
                result = true
            }
        }

        return result
    }

    fun onStop() {

        if (checkInitialization()) {

            if (googleApiClient!!.isConnected) {

                LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, locationListener)
            }

            if (googleApiClient!!.isConnected || googleApiClient!!.isConnecting) {

                googleApiClient!!.disconnect()
                googleApiClient = null
                locationRequest = null
            }
        }
    }

    fun setLogHelper(logHelper: LogHelper) {

        this.logHelper = logHelper
    }

    private fun showErrorDialog(errorCode: Int) {

        GooglePlayServicesHelper.instance.showGooglePlayErrorDialog(appCompatActivity!!, GooglePlayServicesHelper.CONNECTION_FAILURE_RESOLUTION_REQUEST)
    }

    fun addOnLocationHelperListener(locationHelperListener: LocationHelperListener) {

        this.locationHelperListener = locationHelperListener
    }

    private fun checkInitialization(): Boolean {

        return initialized && googleApiClient != null && locationManager != null
    }

    private fun onInitialized(bundle: Bundle) {

        if (locationHelperListener != null) {

            locationHelperListener!!.onInitialized(bundle)
        }
    }

    private fun onConnected() {

        if (DebugHelper.isDebuggable(appCompatActivity)) {

            logHelper.debug(TAG, "LocationHelper connected")
        }

        if (locationHelperListener != null) {

            locationHelperListener!!.onConnected()
        }
    }

    private fun onSuspended(cause: Int) {

        if (DebugHelper.isDebuggable(appCompatActivity)) {

            val message = "LocationHelper suspended - "

            when (cause) {
                GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST ->

                    logHelper.debug(TAG, MessageFormat.format("{0}CAUSE_NETWORK_LOST", message))
                GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED ->

                    logHelper.debug(TAG, MessageFormat.format("{0}CAUSE_SERVICE_DISCONNECTED", message))

                else -> logHelper.debug(TAG, MessageFormat.format("{0}CAUSE_UNKNOWN", message))
            }
        }

        if (locationHelperListener != null) {

            locationHelperListener!!.onLocationSuspended(cause)
        }
    }

    private fun onConnectionFailed() {

        if (DebugHelper.isDebuggable(appCompatActivity)) {

            logHelper.debug(TAG, "LocationHelper connection failed")
        }
        if (locationHelperListener != null) {

            locationHelperListener!!.onConnectionFailed()
        }
    }

    private fun onLocationChanged(newLocation: Location) {

        if (locationHelperListener != null) {

            locationHelperListener!!.onLocationChanged(newLocation)
        }
    }

    companion object {

        private const val TAG = "LocationHelper"

        private const val REQUEST_RESOLVE_ERROR = 1001

        @SuppressLint("StaticFieldLeak")
        var instance: LocationHelper = LocationHelper()
    }
}
