/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.helper

import android.location.Location

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Polyline
import com.google.android.gms.maps.model.PolylineOptions

/**
 * Created by firetrap on 12/10/16.
 */
class PolylineHelper {

    /**
     * Are polylines overlaping boolean.
     *
     * @param polylineLhs the polyline lhs
     * @param polylineRhs the polyline rhs
     * @return the boolean
     */
    fun arePolylinesOverlaping(polylineLhs: Polyline, polylineRhs: Polyline): Boolean {
        val polylineLhsPoints = polylineLhs.points
        val polylineRhsPoints = polylineRhs.points


        for (i in polylineLhsPoints.indices) {

            val latLng1 = polylineLhsPoints[i]
            val latLng2 = polylineRhsPoints[i]

            val location1 = Location("")
            val location2 = Location("")

            location1.latitude = latLng1.latitude
            location1.longitude = latLng1.longitude

            location2.latitude = latLng2.latitude
            location2.longitude = latLng2.longitude

            if (location1.distanceTo(location2) == 0f) {

                return true
            }
        }

        return false
    }

    companion object {

        /**
         * Gets polyline.
         *
         * @param fromPosition the from position
         * @param toPosition   the to position
         * @param zIndex       the z index
         * @param lineColor    the line color
         * @param lineWidth    the line width
         * @return polyline polyline options
         */
        fun drawPolyline(fromPosition: LatLng, toPosition: LatLng, zIndex: Int, lineColor: Int, lineWidth: Float): PolylineOptions {
            val polylineOptions = PolylineOptions()
            polylineOptions.add(fromPosition, toPosition)
            polylineOptions.width(lineWidth)
            polylineOptions.zIndex(zIndex.toFloat())
            polylineOptions.color(lineColor)
            return polylineOptions
        }

        /**
         * Draw dashed polyline polyline options.
         *
         * @param fromPosition the from position
         * @param toPosition   the to position
         * @param lineColor    the line color
         * @param zIndex       the z index
         * @param lineWidth    the line width
         * @return the polyline options
         */
        fun drawDashedPolyline(fromPosition: LatLng, toPosition: LatLng, lineColor: Int, zIndex: Int, lineWidth: Float): PolylineOptions {
            val polylineOptions = PolylineOptions()
            polylineOptions.add(fromPosition, toPosition)
            polylineOptions.width(lineWidth)
            polylineOptions.zIndex(zIndex.toFloat())
            polylineOptions.color(lineColor)

            return polylineOptions
        }
    }
}
