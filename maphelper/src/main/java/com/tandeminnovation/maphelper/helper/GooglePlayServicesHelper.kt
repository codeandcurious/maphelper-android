/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.helper

import android.annotation.SuppressLint
import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.ConnectionResult.*
import com.google.android.gms.common.GoogleApiAvailability
import com.tandeminnovation.appbase.helper.LogHelper

/**
 * Created by firetrap on 28/10/2016.
 */
class GooglePlayServicesHelper private constructor() {

    private val googleAPI = GoogleApiAvailability.getInstance()
    private var logHelper = LogHelper.instance
    var isConnected = false
        private set

    fun servicesConnected(appCompatActivity: AppCompatActivity): Boolean {

        var result = false

        // Check that Google Play services is available
        val resultCode = googleAPI.isGooglePlayServicesAvailable(appCompatActivity)
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {

            logHelper.debug("Location Updates", "Google Play services is available.")
            isConnected = true
            result = true
            // Google Play services was not available for some reason
        } else {
            // Get the error dialog from Google Play services
            showGooglePlayErrorDialog(appCompatActivity, resultCode)
        }

        return result
    }

    fun isGooglePlayServicesResult(appCompatActivity: AppCompatActivity, requestCode: Int, resultCode: Int): Boolean {

        var result = false

        // Decide what to do based on the original request code
        when (requestCode) {
            CONNECTION_FAILURE_RESOLUTION_REQUEST -> {
                result = true
                // If the result code is Activity.RESULT_OK, try to connect again
                when (resultCode) {
                    Activity.RESULT_OK ->
                        // Try the request again
                        servicesConnected(appCompatActivity)
                }
            }
        }

        return result
    }

    @SuppressLint("RestrictedApi")
    fun showGooglePlayErrorDialog(appCompatActivity: AppCompatActivity, resultCode: Int) {

        val errorDialog = googleAPI.getErrorDialog(appCompatActivity, resultCode, CONNECTION_FAILURE_RESOLUTION_REQUEST)
        errorDialog!!.setCanceledOnTouchOutside(false)

        // If Google Play services can provide an error dialog
        // Create a new DialogFragment for the error dialog
        val errorFragment = DialogFragment()
        // Set the dialog in the DialogFragment
        errorFragment.setupDialog(errorDialog, DialogFragment.STYLE_NO_TITLE)
        // Show the error dialog in the DialogFragment
        errorFragment.show(appCompatActivity.supportFragmentManager, "Location Updates")

    }

    fun checkPlayServices(mActivity: Activity): Boolean {
        var cancel = false

        var playServicesError = 0

        if (googleAPI.isGooglePlayServicesAvailable(mActivity) == SERVICE_MISSING) {
            cancel = true
            playServicesError = SERVICE_MISSING
        }

        if (googleAPI.isGooglePlayServicesAvailable(mActivity) == SERVICE_VERSION_UPDATE_REQUIRED) {
            cancel = true
            playServicesError = SERVICE_VERSION_UPDATE_REQUIRED
        }

        if (googleAPI.isGooglePlayServicesAvailable(mActivity) == SERVICE_DISABLED) {
            cancel = true
            playServicesError = SERVICE_DISABLED
        }

        if (cancel) {
            val errorDialog = googleAPI.getErrorDialog(mActivity, playServicesError, CONNECTION_FAILURE_RESOLUTION_REQUEST)
            errorDialog.setCanceledOnTouchOutside(false)
            errorDialog.show()
        }

        return !cancel
    }

    companion object {

        private const val TAG = "GooglePlayServicesHelper"
        internal const val CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000

        var instance: GooglePlayServicesHelper = GooglePlayServicesHelper()

    }
}