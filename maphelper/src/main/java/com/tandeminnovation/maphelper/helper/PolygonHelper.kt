/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.helper

import android.location.Location

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Polygon
import com.google.android.gms.maps.model.PolygonOptions

import java.util.ArrayList

/**
 * Created by firetrap on 12/10/16.
 */
object PolygonHelper {

    /**
     * Draw box around region
     *
     * @param bound     the bound
     * @param lineWidth the line width
     * @param zIndex    the z index
     * @param lineColor the line color
     * @return the bounding box polygon
     */
    fun drawBoundingBoxPolygon(bound: LatLngBounds, lineWidth: Float, zIndex: Int, lineColor: Int): PolygonOptions {
        val polygonOptions = PolygonOptions()

        polygonOptions.add(LatLng(bound.southwest.latitude, bound.southwest.longitude))
        polygonOptions.add(LatLng(bound.southwest.latitude, bound.northeast.longitude))
        polygonOptions.add(LatLng(bound.northeast.latitude, bound.northeast.longitude))
        polygonOptions.add(LatLng(bound.northeast.latitude, bound.southwest.longitude))

        polygonOptions.strokeWidth(lineWidth)
        polygonOptions.strokeColor(lineColor)
        polygonOptions.zIndex(zIndex.toFloat())

        return polygonOptions
    }


    /**
     * Draw full filled polygon polygon options.
     *
     * @param polygonPositions the polygon positions
     * @param strokeColor      the stroke color
     * @param strokeWidth      the stroke width
     * @param fillColor        the fill color
     * @return the polygon options
     */
    fun drawFullFilledPolygon(polygonPositions: ArrayList<LatLng>, strokeColor: Int, strokeWidth: Float, fillColor: Int): PolygonOptions {
        val polygonOptions = PolygonOptions()
        polygonOptions.addAll(polygonPositions)
        polygonOptions.strokeColor(strokeColor)
        polygonOptions.strokeWidth(strokeWidth)
        polygonOptions.fillColor(fillColor)

        return polygonOptions
    }

    /**
     * Are in same place boolean.
     *
     * @param polygonLhs the polygon lhs
     * @param polygonRhs the polygon rhs
     * @return the boolean
     */
    fun arePolygonsInSamePlace(polygonLhs: Polygon, polygonRhs: Polygon): Boolean {
        val polygonLhsPoints = polygonLhs.points
        val polygonRhsPoints = polygonRhs.points

        for (i in polygonLhsPoints.indices) {

            val latLng1 = polygonLhsPoints[i]
            val latLng2 = polygonRhsPoints[i]

            val location1 = Location("")
            val location2 = Location("")

            location1.latitude = latLng1.latitude
            location1.longitude = latLng1.longitude

            location2.latitude = latLng2.latitude
            location2.longitude = latLng2.longitude

            if (location1.distanceTo(location2) > 0) {

                return false
            }
        }

        return true
    }
}
