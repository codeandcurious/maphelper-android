/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper

import com.google.android.gms.location.LocationRequest

/**
 * Created by firetrap on 14/12/2015.
 */
object Constants {

    /**
     * The constant DEFAULT_LOCATION_PRIORITY.
     */
    const val DEFAULT_LOCATION_PRIORITY = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY

    /**
     * The constant DEFAULT_LOCATION_UPDATE_INTERVAL.
     */
    const val DEFAULT_LOCATION_UPDATE_INTERVAL = 5 //SECONDS

    /**
     * The constant SELECTED_MAPPOI_ID_STATE_KEY.
     */
    const val SELECTED_MAPPOI_ID_STATE_KEY = "SelectedMarkerIdStateKey"

    /**
     * The constant CAMERA_POSITION_STATE_KEY.
     */
    const val CAMERA_POSITION_STATE_KEY = "CameraPositionStateKey"

}
