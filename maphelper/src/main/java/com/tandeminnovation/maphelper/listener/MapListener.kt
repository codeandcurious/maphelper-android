/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.listener

import com.tandeminnovation.maphelper.model.MapPoi

/**
 * Created by firetrap on 30/09/16.
 */
interface MapListener {

    /**
     * On map poi click.
     *
     * @param mapPoi the map poi
     */
    fun onMapPoiClick(mapPoi: MapPoi)

    /**
     * On map click.
     */
    fun onMapClick()

    /**
     * On map ready.
     */
    fun onMapReady()

}
