/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.listener

import android.location.Location
import android.os.Bundle

/**
 * Created by firetrap on 29/09/16.
 */
interface LocationHelperListener {

    /**
     * On initialized.
     *
     * @param bundle the bundle
     */
    fun onInitialized(bundle: Bundle)

    /**
     * On connected.
     */
    fun onConnected()

    /**
     * On disconnected.
     */
    fun onDisconnected()

    /**
     * On connection failed.
     */
    fun onConnectionFailed()

    /**
     * On location suspended.
     *
     * @param cause the cause
     */
    fun onLocationSuspended(cause: Int)

    /**
     * On location changed.
     *
     * @param location the new location
     */
    fun onLocationChanged(location: Location)
}
