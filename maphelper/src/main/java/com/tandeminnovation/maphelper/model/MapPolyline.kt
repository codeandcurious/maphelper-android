/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.model

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Polyline
import com.google.android.gms.maps.model.PolylineOptions
import kotlin.properties.Delegates

/**
 * Created by firetrap on 14/12/2015.
 */
class MapPolyline {

    var id: Long by Delegates.notNull()
    private var latLngs: Array<LatLng>? = null
    var polylineOptions: PolylineOptions? = null
        get() {
            if (field == null) {

                this.polylineOptions = PolylineOptions()
                field!!.add(*this.latLngs!!)
                field!!.width(this.width)
                field!!.zIndex(this.zIndex)
                field!!.color(this.color)
            }

            return field
        }
    var polyline: Polyline? = null
    var color = -16777216
    var zIndex = 0.0f
    var width = 10.0f
    var isActive = true

    constructor(vararg latLngs: LatLng) {

        this.latLngs = arrayOf(*latLngs)
    }

    constructor(latLngs: List<LatLng>) {
        this.latLngs = latLngs.toTypedArray()
        MapPolygon(LatLng(12.0, 12.0), LatLng(123.0, 123.0))
    }
}
