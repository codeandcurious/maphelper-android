/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.model

import android.graphics.Bitmap
import android.location.Location
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*

/**
 * Created by firetrap on 14/12/2015.
 */
class MapPoi {

    var id: Long = View.generateViewId().toLong()
    var title: String? = null
    var location: Location? = null
    var distance: Double = 0.toDouble()
    var activationRadius: Float? = null

    var isActive = true
    var isVisited: Boolean = false
    var isSelected: Boolean = false
    private var containsInfoWindow: Boolean = false
    var infoWindowAnchor: FloatArray? = null
    var infoWindowAdapter: GoogleMap.InfoWindowAdapter? = null
    private var markerOptions: MarkerOptions? = null
    var marker: Marker? = null
    private var unselectedIcon: BitmapDescriptor? = null
    private var selectedIcon: BitmapDescriptor? = null
    private var inactiveIcon: BitmapDescriptor? = null
    private var visitedIcon: BitmapDescriptor? = null
    private var selectedVisitedIcon: BitmapDescriptor? = null
    private var unselectedVisitedIcon: BitmapDescriptor? = null
    private var mapPoiIcon: MapPoiIcon? = null
    private var anchor: FloatArray? = null
    var tag: Any? = null

    val latitude: Double?
        get() = location!!.latitude

    val longitude: Double?
        get() = location!!.longitude

    constructor(location: Location) {
        this.location = location
        this.mapPoiIcon = MapPoiIcon()
    }

    constructor(latitude: Double, longitude: Double) {
        location = Location("")
        location!!.latitude = latitude
        location!!.longitude = longitude
        this.mapPoiIcon = MapPoiIcon()
    }

    constructor(latitude: Double, longitude: Double, altitude: Double) {
        location = Location("")
        location!!.altitude = altitude
        location!!.latitude = latitude
        location!!.longitude = longitude
        this.mapPoiIcon = MapPoiIcon()
    }

    constructor(latLng: LatLng) {
        this.location = Location("")
        this.location!!.latitude = latLng.latitude
        this.location!!.longitude = latLng.longitude
        this.mapPoiIcon = MapPoiIcon()
    }

    constructor(latLng: LatLng, altitude: Double) {
        this.location = Location("")
        this.location!!.latitude = latLng.latitude
        this.location!!.longitude = latLng.longitude
        this.location!!.altitude = altitude
        this.mapPoiIcon = MapPoiIcon()
    }

    fun getMarkerOptions(): MarkerOptions {
        if (markerOptions == null) {

            markerOptions = MarkerOptions()
            markerOptions!!.position(LatLng(latitude!!, longitude!!))
            markerOptions!!.title(title)

        }

        return markerOptions as MarkerOptions
    }

    fun getAnchor(): FloatArray {
        if (anchor == null) {

            this.anchor = FloatArray(2)
            //Center on marker
            this.anchor!![0] = 0.5f
            this.anchor!![1] = 1.0f
        }

        return anchor as FloatArray
    }

    fun setAnchor(leftPercentage: Float, topPercentage: Float) {
        this.anchor = FloatArray(2)
        this.anchor!![0] = leftPercentage
        this.anchor!![1] = topPercentage
    }


    fun showInfoWindow() {

        this.marker!!.showInfoWindow()
    }

    fun getUnselectedIcon(): BitmapDescriptor? {

        return bitmapDescriptorBuilder(this.unselectedIcon, mapPoiIcon!!.unselectedResId, mapPoiIcon!!.unselectedBitmap)

    }

    fun setUnselectedIcon(unselectedResId: Int) {

        this.mapPoiIcon!!.unselectedResId = unselectedResId
    }

    fun setUnselectedIcon(unselectedBitmap: Bitmap) {

        this.mapPoiIcon!!.unselectedBitmap = unselectedBitmap
    }

    fun setUnselectedIcon(unselectedBitmapDescriptor: BitmapDescriptor) {

        this.unselectedIcon = unselectedBitmapDescriptor
    }

    fun getSelectedIcon(): BitmapDescriptor? {

        return bitmapDescriptorBuilder(this.selectedIcon, mapPoiIcon!!.selectedResId, mapPoiIcon!!.selectedBitmap)
    }

    fun setSelectedIcon(selectedResId: Int) {

        this.mapPoiIcon!!.selectedResId = selectedResId
    }

    fun setSelectedIcon(selectedBitmap: Bitmap) {

        this.mapPoiIcon!!.selectedBitmap = selectedBitmap
    }

    fun setSelectedIcon(selectedBitmapDescriptor: BitmapDescriptor) {

        this.selectedIcon = selectedBitmapDescriptor
    }

    fun getInactiveIcon(): BitmapDescriptor? {

        return bitmapDescriptorBuilder(this.inactiveIcon, mapPoiIcon!!.inactiveResId, mapPoiIcon!!.inactiveBitmap)
    }

    fun setInactiveIcon(inactiveResId: Int) {

        this.mapPoiIcon!!.inactiveResId = inactiveResId
    }

    fun setInactiveIcon(inactiveBitmap: Bitmap) {

        this.mapPoiIcon!!.inactiveBitmap = inactiveBitmap
    }

    fun setInactiveIcon(inactiveBitmapDescriptor: BitmapDescriptor) {

        this.inactiveIcon = inactiveBitmapDescriptor
    }

    fun getVisitedIcon(): BitmapDescriptor? {

        return bitmapDescriptorBuilder(this.visitedIcon, mapPoiIcon!!.visitedResId, mapPoiIcon!!.visitedBitmap)
    }

    fun setVisitedIcon(visitedResId: Int) {

        this.mapPoiIcon!!.visitedResId = visitedResId
    }

    fun setVisitedIcon(visitedBitmap: Bitmap) {

        this.mapPoiIcon!!.visitedBitmap = visitedBitmap
    }

    fun setVisitedIcon(visitedBitmapDescriptor: BitmapDescriptor) {

        this.visitedIcon = visitedBitmapDescriptor
    }

    fun getSelectedVisitedIcon(): BitmapDescriptor? {

        return bitmapDescriptorBuilder(this.selectedVisitedIcon, mapPoiIcon!!.selectedVisitedResId, mapPoiIcon!!.selectedVisitedBitmap)
    }

    fun setSelectedVisitedIcon(selectedVisitedResId: Int) {

        this.mapPoiIcon!!.selectedVisitedResId = selectedVisitedResId
    }

    fun setSelectedVisitedIcon(selectedVisitedBitmap: Bitmap) {

        this.mapPoiIcon!!.selectedVisitedBitmap = selectedVisitedBitmap
    }

    fun setSelectedVisitedIcon(selectedVisitedBitmapDescriptor: BitmapDescriptor) {

        this.selectedVisitedIcon = selectedVisitedBitmapDescriptor
    }

    fun getUnselectedVisitedIcon(): BitmapDescriptor? {

        return bitmapDescriptorBuilder(this.unselectedVisitedIcon, mapPoiIcon!!.unselectedVisitedResId, mapPoiIcon!!.unselectedVisitedBitmap)
    }

    fun setUnselectedVisitedIcon(unselectedVisitedResId: Int) {

        this.mapPoiIcon!!.unselectedVisitedResId = unselectedVisitedResId
    }

    fun setUnselectedVisitedIcon(unselectedVisitedBitmap: Bitmap) {

        this.mapPoiIcon!!.unselectedVisitedBitmap = unselectedVisitedBitmap
    }

    fun setUnselectedVisitedIcon(unselectedVisitedBitmapDescriptor: BitmapDescriptor) {

        this.unselectedVisitedIcon = unselectedVisitedBitmapDescriptor
    }

    fun containsInfoWindow(): Boolean {

        return containsInfoWindow
    }

    fun setContainsInfoWindow(containsInfoWindow: Boolean) {

        this.containsInfoWindow = containsInfoWindow
    }

    private fun bitmapDescriptorBuilder(bitmapDescriptor: BitmapDescriptor?, markerIconResId: Int?, markerIconBitmap: Bitmap?): BitmapDescriptor? {
        var mBitmapDescriptor = bitmapDescriptor
        if (mBitmapDescriptor == null) {

            if (markerIconResId != null) {

                mBitmapDescriptor = BitmapDescriptorFactory.fromResource(markerIconResId)
            } else if (markerIconBitmap != null) {

                mBitmapDescriptor = BitmapDescriptorFactory.fromBitmap(markerIconBitmap)
            }
        }

        return mBitmapDescriptor
    }
}
