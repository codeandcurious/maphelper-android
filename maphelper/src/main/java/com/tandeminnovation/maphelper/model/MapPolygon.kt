/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.model

import android.view.View
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Polygon
import com.google.android.gms.maps.model.PolygonOptions

/**
 * Created by firetrap on 14/12/2015.
 */
class MapPolygon {

    var id: Long = View.generateViewId().toLong()
    private var latLngs: Array<LatLng>? = null
    var polygon: Polygon? = null
    var strokeColor = -16777216
    var fillColor = -16777216
    private var zIndex = 0.0f
    var strokeWidth = 10.0f
    var isActive = true
    var polygonOptions: PolygonOptions? = null
        get() {
            if (field == null) {

                this.polygonOptions = PolygonOptions()
                field!!.add(*this.latLngs!!)
                field!!.strokeColor(this.strokeColor)
                field!!.strokeWidth(this.strokeWidth)
                field!!.fillColor(this.fillColor)
                field!!.zIndex(this.zIndex)
            }

            return field
        }

    constructor(vararg latLngs: LatLng) {

        this.latLngs = arrayOf(*latLngs)
    }

    constructor(latLngs: List<LatLng>) {

        this.latLngs = latLngs.toTypedArray()
    }

    fun getzIndex(): Float {
        return zIndex
    }

    fun setzIndex(zIndex: Float) {
        this.zIndex = zIndex
    }
}
