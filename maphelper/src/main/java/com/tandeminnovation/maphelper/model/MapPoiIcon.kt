/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.model

import android.graphics.Bitmap
import android.os.Parcel
import android.os.Parcelable

/**
 * Created by firetrap on 14/12/2015.
 */
class MapPoiIcon : Parcelable {

    var unselectedResId: Int? = null
    var selectedResId: Int? = null
    var inactiveResId: Int? = null
    var visitedResId: Int? = null
    var selectedVisitedResId: Int? = null
    var unselectedVisitedResId: Int? = null
    var unselectedBitmap: Bitmap? = null
    var selectedBitmap: Bitmap? = null
    var inactiveBitmap: Bitmap? = null
    var visitedBitmap: Bitmap? = null
    var selectedVisitedBitmap: Bitmap? = null
    var unselectedVisitedBitmap: Bitmap? = null

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeValue(this.unselectedResId)
        dest.writeValue(this.selectedResId)
        dest.writeValue(this.inactiveResId)
        dest.writeValue(this.visitedResId)
        dest.writeValue(this.selectedVisitedResId)
        dest.writeValue(this.unselectedVisitedResId)
        dest.writeParcelable(this.unselectedBitmap, flags)
        dest.writeParcelable(this.selectedBitmap, flags)
        dest.writeParcelable(this.inactiveBitmap, flags)
        dest.writeParcelable(this.visitedBitmap, flags)
        dest.writeParcelable(this.selectedVisitedBitmap, flags)
        dest.writeParcelable(this.unselectedVisitedBitmap, flags)
    }

    constructor() {}

    protected constructor(`in`: Parcel) {
        this.unselectedResId = `in`.readValue(Int::class.java.classLoader) as Int
        this.selectedResId = `in`.readValue(Int::class.java.classLoader) as Int
        this.inactiveResId = `in`.readValue(Int::class.java.classLoader) as Int
        this.visitedResId = `in`.readValue(Int::class.java.classLoader) as Int
        this.selectedVisitedResId = `in`.readValue(Int::class.java.classLoader) as Int
        this.unselectedVisitedResId = `in`.readValue(Int::class.java.classLoader) as Int
        this.unselectedBitmap = `in`.readParcelable(Bitmap::class.java.classLoader)
        this.selectedBitmap = `in`.readParcelable(Bitmap::class.java.classLoader)
        this.inactiveBitmap = `in`.readParcelable(Bitmap::class.java.classLoader)
        this.visitedBitmap = `in`.readParcelable(Bitmap::class.java.classLoader)
        this.selectedVisitedBitmap = `in`.readParcelable(Bitmap::class.java.classLoader)
        this.unselectedVisitedBitmap = `in`.readParcelable(Bitmap::class.java.classLoader)
    }

    companion object {

        val CREATOR: Parcelable.Creator<MapPoiIcon> = object : Parcelable.Creator<MapPoiIcon> {
            override fun createFromParcel(source: Parcel): MapPoiIcon {
                return MapPoiIcon(source)
            }

            override fun newArray(size: Int): Array<MapPoiIcon?> {
                return arrayOfNulls(size)
            }
        }
    }
}
