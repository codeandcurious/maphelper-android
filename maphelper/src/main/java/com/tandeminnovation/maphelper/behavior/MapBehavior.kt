/*
 *   Copyright 2018 Tandem Innovation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.tandeminnovation.maphelper.behavior

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Bundle
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.tandeminnovation.maphelper.Constants
import com.tandeminnovation.maphelper.config.MapConfig
import com.tandeminnovation.maphelper.helper.MapHelper
import com.tandeminnovation.maphelper.helper.MarkerHelper
import com.tandeminnovation.maphelper.manager.MapPoiManager
import com.tandeminnovation.maphelper.manager.MapPolygonManager
import com.tandeminnovation.maphelper.manager.MapPolylineManager
import com.tandeminnovation.maphelper.model.MapPoi
import com.tandeminnovation.maphelper.model.MapPolygon
import com.tandeminnovation.maphelper.model.MapPolyline

/**
 * Created by firetrap on 28/10/2016.
 */
class MapBehavior(mapHelper: MapHelper, mapConfig: MapConfig, private val context: Context, val savedInstanceState: Bundle?) {

    var isPinActivationEnabled = false
    var isPinVisitEnabled = false
    var isZoomOutUntilNearestPoiEnabled = true
        private set

    var mapConfig: MapConfig? = null
    var location: Location? = null
    var googleMap: GoogleMap? = null
    var mapHelper: MapHelper? = null
    var mapPoiManager: MapPoiManager<Any>? = null
        set(mapPoiManager) {
            field = mapPoiManager
            this.mapPoiManager!!.setDataObserver(mapPoiDataObserver)
        }
    var mapPolylineManager: MapPolylineManager<Any>? = null
        set(mapPolylineManager) {
            field = mapPolylineManager
            this.mapPolylineManager!!.setDataObserver(mapPolylineDataObserver)

        }
    var mapPolygonManager: MapPolygonManager<Any>? = null
        set(mapPolygonManager) {
            field = mapPolygonManager
            this.mapPolygonManager!!.setDataObserver(mapPolygonDataObserver)
        }

    private val mapPoiDataObserver: MapPoiManager.MapPoiDataObserver = object : MapPoiManager.MapPoiDataObserver {

        override fun dataSetChanged() {

            onPreExecuteMapPoiBehavior()
        }
    }

    private val mapPolylineDataObserver: MapPolylineManager.MapPolylineDataObserver = object : MapPolylineManager.MapPolylineDataObserver {

        override fun dataSetChanged() {

            onPreExecuteMapPolylineBehavior()
        }
    }

    private val mapPolygonDataObserver: MapPolygonManager.MapPolygonDataObserver = object : MapPolygonManager.MapPolygonDataObserver {

        override fun dataSetChanged() {

            onPreExecuteMapPolygonBehavior()
        }
    }

    init {
        this.mapHelper = mapHelper
        this.mapConfig = mapConfig
    }

    fun onPreExecuteMapPoiBehavior() {
        //The objects to transform
        val items = this.mapPoiManager!!.objects

        //List<Objects> to List<MapPoi>
        val mapPoiList: List<MapPoi>?
        mapPoiList = this.mapPoiManager!!.transform(items)

        //If savedInstanceState is null it's the first time placing markers and do the default behaviour
        if (savedInstanceState == null) {

            applyMapPoiInitialBehavior(mapPoiList)
        } else {

            applyMapPoiSavedStateBehavior(mapPoiList)
        }
    }

    fun onPreExecuteMapPolylineBehavior() {
        //The objects to transform
        val item = this.mapPolylineManager!!.`object`

        var mapPolyline: MapPolyline? = null
        if (item != null) {

            mapPolyline = this.mapPolylineManager!!.transform(item)
        }
        mapHelper!!.addPolylinesToMap(mapPolyline)
    }

    fun onPreExecuteMapPolygonBehavior() {
        //The objects to transform
        val items = this.mapPolygonManager!!.objects

        if (items.isNotEmpty()) {

            var mapPolygon: MapPolygon?

            for (item in items) {

                mapPolygon = this.mapPolygonManager!!.transform(item)
                mapHelper!!.addPolygonToMap(mapPolygon)
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun applyMapPoiInitialBehavior(mapPoiList: List<MapPoi>?) {
        if (mapConfig!!.isCenterOnUserEnabled) {

            if (MapHelper.checkPermissions(context, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)) {

                googleMap!!.isMyLocationEnabled = true
            }
        }

        mapHelper!!.addMarkers(mapPoiList, null)
    }

    private fun applyMapPoiSavedStateBehavior(mapPoiList: List<MapPoi>?) {
        var savedInstanceMapPoiId: Long? = null
        if (savedInstanceState!!.containsKey(Constants.SELECTED_MAPPOI_ID_STATE_KEY)) {

            savedInstanceMapPoiId = savedInstanceState.getLong(Constants.SELECTED_MAPPOI_ID_STATE_KEY)
        }

        if (savedInstanceState.containsKey(Constants.CAMERA_POSITION_STATE_KEY)) {

            val cameraPosition = savedInstanceState.getParcelable<CameraPosition>(Constants.CAMERA_POSITION_STATE_KEY)
            if (cameraPosition != null) {

                mapHelper!!.centerCameraOnPosition(cameraPosition, false)
                mapHelper!!.isFirstLocationUpdate = false
            }
        }

        mapHelper!!.addMarkers(mapPoiList, savedInstanceMapPoiId)
    }

    fun onPostExecuteBehaviour(nearestMapPoi: MapPoi) {

        zoomOutToShowClosestMarker(nearestMapPoi)
    }

    fun setZoomOutUntilNearestPoi(zoomOutUntilNearestPoi: Boolean) {
        this.isZoomOutUntilNearestPoiEnabled = zoomOutUntilNearestPoi
    }

    private fun centerCameraOnPosition(location: Location, zoomLevel: Float, animate: Boolean) {
        val zoomToMe = CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), zoomLevel)
        if (animate) {

            googleMap!!.animateCamera(zoomToMe)
        } else {

            googleMap!!.moveCamera(zoomToMe)
        }
    }

    private fun zoomOutToShowClosestMarker(nearestMapPoi: MapPoi?) {
        if (isZoomOutUntilNearestPoiEnabled && nearestMapPoi != null) {

            val nearestCoordinates = LatLng(nearestMapPoi.location!!.latitude, nearestMapPoi.location!!.longitude)

            val builder = LatLngBounds.Builder()
            builder.include(googleMap!!.cameraPosition.target)
            builder.include(nearestCoordinates)
            builder.include(MarkerHelper.getSymmetricCoordinates(googleMap!!.cameraPosition.target, nearestCoordinates))
            val bounds = builder.build()

            // only zoom out, no zoom in
            if (!googleMap!!.projection.visibleRegion.latLngBounds.contains(bounds.northeast)) {

                // tilt and rotation
                val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, ZOOM_OUT_PIN_MARGINS)
                googleMap!!.animateCamera(cameraUpdate)
            }
        }
    }

    companion object {

        private val TAG = "MapBehavior"

        private val ZOOM_OUT_PIN_MARGINS = 100
    }
}
